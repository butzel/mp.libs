#####  Grove 1602 RGB
Micropython library for Grove-LCD-RGB Backlight

#### Example:
``` Python 
# I2C on ESP8266
from machine import I2C, Pin
i2c = I2C(sda=Pin(4, Pin.OUT), scl=Pin(5, Pin.OUT))

import lcds.grove as grove
lcd = grove.LCD_RGB_Backlight(i2c)
lcd.bl_on()
lcd.home()
lcd.write("I \x03 MicroPython\x01")
lcd.cursorMode(1)

# define some own char's
pxl = [0, 1, 2, 3, 4, 5, 6, 7]
pxl[0] = [0, 0, 0, 0, 0]
pxl[1] = [0, 1, 0, 1, 0]
pxl[2] = [0, 1, 0, 1, 0]
pxl[3] = [0, 0, 0, 0, 0]
pxl[4] = [0, 0, 0, 0, 0]
pxl[6] = [1, 0, 0, 0, 1]
pxl[5] = [0, 1, 1, 1, 0]
pxl[7] = [0, 0, 0, 0, 0]
lcd.saveChar(0, pxl)

pxl[0] = [0, 0, 0, 0, 0]
pxl[1] = [0, 1, 0, 1, 0]
pxl[2] = [0, 1, 0, 1, 0]
pxl[3] = [0, 0, 0, 0, 0]
pxl[4] = [0, 0, 0, 0, 0]
pxl[5] = [1, 0, 0, 0, 1]
pxl[6] = [0, 1, 1, 1, 0]
pxl[7] = [0, 0, 0, 0, 0]
lcd.saveChar(1, pxl)

pxl[0] = [0, 0, 0, 0, 0]
pxl[1] = [0, 0, 0, 0, 0]
pxl[2] = [0, 0, 0, 0, 1]
pxl[3] = [0, 0, 0, 1, 0]
pxl[4] = [1, 0, 1, 0, 0]
pxl[5] = [0, 1, 0, 0, 0]
pxl[6] = [0, 0, 0, 0, 0]
pxl[7] = [0, 0, 0, 0, 0]
lcd.saveChar(2, pxl)

pxl[0] = [0, 0, 0, 0, 0]
pxl[1] = [0, 1, 0, 1, 0]
pxl[2] = [1, 1, 1, 1, 1]
pxl[3] = [1, 1, 1, 1, 1]
pxl[4] = [1, 1, 1, 1, 1]
pxl[5] = [0, 1, 1, 1, 0]
pxl[6] = [0, 0, 1, 0, 0]
pxl[7] = [0, 0, 0, 0, 0]
lcd.saveChar(3, pxl)


# Power management
pxl[0] = [0, 0, 0, 0, 0]
pxl[1] = [0, 1, 0, 1, 0]
pxl[2] = [0, 1, 0, 1, 0]
pxl[3] = [1, 1, 1, 1, 1]
pxl[4] = [1, 1, 1, 1, 1]
pxl[5] = [0, 1, 1, 1, 0]
pxl[6] = [0, 0, 1, 0, 0]
pxl[7] = [0, 0, 0, 1, 1]
lcd.saveChar(4, pxl)

pxl[0] = [0, 1, 1, 0, 0]
pxl[1] = [1, 1, 1, 1, 0]
pxl[2] = [1, 1, 1, 1, 0]
pxl[3] = [1, 1, 1, 1, 0]
pxl[4] = [1, 1, 1, 1, 0]
pxl[5] = [1, 1, 1, 1, 0]
pxl[6] = [1, 1, 1, 1, 0]
pxl[7] = [1, 1, 1, 1, 0]
lcd.saveChar(5, pxl)

pxl[0] = [0, 1, 1, 0, 0]
pxl[1] = [1, 1, 1, 1, 0]
pxl[2] = [1, 0, 0, 1, 0]
pxl[3] = [1, 0, 0, 1, 0]
pxl[4] = [1, 1, 1, 1, 0]
pxl[5] = [1, 1, 1, 1, 0]
pxl[6] = [1, 1, 1, 1, 0]
pxl[7] = [1, 1, 1, 1, 0]
lcd.saveChar(6, pxl)

pxl[0] = [0, 1, 1, 0, 0]
pxl[1] = [1, 1, 1, 1, 0]
pxl[2] = [1, 0, 0, 1, 0]
pxl[3] = [1, 0, 0, 1, 0]
pxl[4] = [1, 0, 0, 1, 0]
pxl[5] = [1, 0, 0, 1, 0]
pxl[6] = [1, 0, 0, 1, 0]
pxl[7] = [1, 1, 1, 1, 0]
lcd.saveChar(7, pxl)

# battery play time
def bat(lcd, energy):
    lcd.bl_blink(False)
    if energy == 0:
        lcd.bl_color(255)
        lcd.bl_blink()
    elif energy <= 3:
        lcd.bl_color(0, 0, 255)
    elif energy == 2:
        lcd.bl_color(0, 255)
    else:
        lcd.bl_color(255, 128)
    energy = 7 - (energy % 4)
    lcd.charAt(energy, (15, 1))

# wrong clock
def clock(lcd=lcd):
    accu = 4
    for i in range(60):
        __import__("time").sleep(.5)
        if i % 15 == 0:
            accu -= 1
        bat(lcd, accu)
        dt = __import__("machine").RTC().datetime()
        lcd.cursor(3, 1)
        lcd.write(str(dt[4]) + ":" + str(dt[5]) + ":" + str(dt[6]))
    bat(lcd, 3)


clock()
```

#### Methods
### Backlight
- bl_on()
  - turn backlight on
- bl_off()
  - turn backlight off
- bl_color(red, green, blue)
  - set the backlight color 
- bl_blink(duty, freq)
   - active backlights blink mode
   - freq: frequency 
   - duty: duration (0=off,1=short, 255=full)
- bl_stopblink()
  - stop blinking Backlight

### Cursor
- cursormode(cursor)
  - set the cursormode
  - cursor = 0: cursor off
  - cursor = -1/1 blink off/on
  - cursor = -2/2 underscore off/on
- home()
  - move the Cursor to HOME position

### LCD
- clear()
  - clear screen
- write(txt,pos)
  - write txt to LCD at pos-ition
  - txt: string with text
  - pos: tuple with coordinates or False
- charAt(char, pos)
  - writes one character ath the desired pos-ition
  - char: character
  - pos: tuple with coordinates
- saveChar(char, pxl)
  - define a custom-char
  - char: integer memoryslot
  - pxl: 2d boolean lst with pixels

