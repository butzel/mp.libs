__author__ = 'butzel'
__license__ = "GPLv3: https://www.gnu.org/licenses/gpl-3.0.en.html"
__version__ = "0.3"
from .st7735 import ST7735


class ILI9163(ST7735):
    """ dirty hack for this display """

    def init(self):
        """
        initialize the Display
        """
        super().init()
        self.rotation(1, 1)

    def color(self, r, g, b):
        """ returns the color-value
        r: red 0-255
        g: green 0-255
        b: blue 0-255"""
        return super().color(r, b, g)
