__author__ = 'butzel'
__license__ = "GPLv3: https://www.gnu.org/licenses/gpl-3.0.en.html"
__version__ = "0.3"
from time import sleep


class ST7735(object):
    HIGH = 1
    LOW = 0
    # COMMANDs
    CMD_SW_RESET = b'\x01'
    CMD_SLEEP_IN = b'\x10'
    CMD_SLEEP_OUT = b'\x11'

    CMD_INVERSON_OFF = b'\x20'
    CMD_INVERSON_ON = b'\x21'

    CMD_DISPLAY_OFF = b'\x28'
    CMD_DISPLAY_ON = b'\x29'
    CMD_SET_COLUMN = b'\x2A'
    CMD_SET_ROW = b'\x2B'
    CMD_WRITE_MEM = b'\x2C'
    CMD_REFRESH_ORDER = b'\x36'
    CMD_IDLE_OFF = b'\x38'
    CMD_IDLE_ON = b'\x39'
    CMD_COLMOD = b'\x3A'

    # check'
    CMD_SET_COLOR = b'\x2D'
    CMD_SET_BRIGHT = b'\x52'

    OPT_CLR_12 = 3  # 12bit per pixel
    OPT_CLR_16 = 5  # 16bit per pixel
    OPT_CLR_18 = 6  # 18bit per pixel

    def __init__(self, spi, dc, cs, rst, w, h, fb=1, init=1, offset=(0, 0)):
        """
        creates ili9341 object
        spi:  - SPI-Object (machine.pin)
        dc: digital pin  (Data/Command)
        optional keywords arguments
        cs: digital pin  (Cable-Select)
        rst: digital Pin  (Reset)
        w: display width
        h: display height
        fb: use framebuffer
        init: initialize (init)
        offset: Display Pixel-Offset
        """
        self._spi = spi
        self._dc = dc
        self._cs = cs
        self._rst = rst
        self.__offset = offset[0] | offset[1]
        self._oX = offset[0]
        self._oY = offset[1]
        self._width = w
        self._height = h
        self._order_TB = not True
        self._order_LR = not True
        self._order_RGB = not True
        self._pxlFormat = ST7735.OPT_CLR_16
        self._mirror = 192  # 11000000
        self.__fb = None
        if init:
            self.init()

        if fb:
            fb = __import__("framebuf")
            self.__fb_puf = bytearray(w * h * 2)  # 2bytes RGB
            if self.__offset:
                self.__offset = bytearray(w * h * 2)
                self.__fb = fb.FrameBuffer(self.__offset, w, h, fb.RGB565)
                self.__offset = fb.FrameBuffer(self.__fb_puf, w, h, fb.RGB565)
            else:
                self.__fb = fb.FrameBuffer(self.__fb_puf, w, h, fb.RGB565)
            self._pxlFormat = ST7735.OPT_CLR_16
            # enable extra features ;)
            self.fill_rect = self.__fb.fill_rect
            self.fill = self.__fb.fill
            self.pixel = self.__fb.pixel
            self.text = self.__fb.text
            self.scroll = self.__fb.scroll
            self.vline = self.__fb.vline
            self.hline = self.__fb.hline
            self.line = self.__fb.line

    @staticmethod
    @micropython.native
    def color_12(r, g, b):
        """
        transfrom to 12-bit color values:
        """
        return ((r//16) << 8) + ((g//16) << 4) + (b//16)

    @staticmethod
    @micropython.native
    def color_16(r, g, b):
        """
        transform to 16-bit color values:
        """
        return ((r//8) << 11) + ((g//4) << 5) + (b//8)

    @staticmethod
    @micropython.native
    def color_18(r, g, b):
        """
        transform to 16-bit color values:
        """
        return ((r//4) << 12) + ((g//4) << 6) + (b//4)

    @micropython.native
    def color(self, r, g, b):
        """ returns the color-value
        r: red 0-255
        g: green 0-255
        b: blue 0-255"""
        # Dont call the static methods  ~13% slower
        # also shift is slower
        pxl = self._pxlFormat
        if pxl == ST7735.OPT_CLR_12:
            return ((r//16) * 256) + ((g//16) * 16) + (b//16)
        elif pxl == ST7735.OPT_CLR_16:
            return ((r//8) * 2048) + ((g//4) * 32) + (b//8)
        elif pxl == ST7735.OPT_CLR_18:
            return ((r//4) * 4096) + ((g//4) * 64) + (b//4)
        else:
            return (r << 16) + (g << 8) + b  # shift is slow!

    def rate(self, dta):
        """
        Calculate the Frame Rate 10.2.2
        TODO: dynamic
        """
        vals = []
        for i in range(len(dta)):
            vals.append(dta[i])
        rtna, fpa, bpa = vals
        rtna = ord(rtna)
        fpa = ord(fpa)
        bpa = ord(bpa)
        return (333 * 1000) / ((rtna+20) * (self._wait + fpa + bpa))

    def _wait(self, sec):
        """
        wait seconds
        sec: float seconds
        """
        sleep(sec)

    @micropython.native
    def _send(self, cmd=None, dta=None):
        """ Send messages to display
        cmd: command
        dta: data
        if both passed, command is sent first
        """
        if self._cs:
            self._cs.value(ST7735.LOW)  # Low is active!

        if cmd:  # Command
            self._dc.value(ST7735.LOW)
            self._spi.write(cmd)
        if dta:  # DATA
            self._dc.value(ST7735.HIGH)
            self._spi.write(dta)

        if self._cs:
            self._cs.value(ST7735.HIGH)

    def _read(self, cmd, dta, answer):
        # no miso, no data
        return None

    def init(self):
        """
        initialize the Display
        """
        self.reset(init=False)  # RESET to be safe
        self.sleep(False)

        # Frame Rate
        self._send(b'\xb2', b'\x02,\x2D\x2E')
        self._send(b'\xb3', b'\x02\x2d\x2e\x02\x2d\x2e')

        # Voltages
        self._send(b'\xc0', b'\x02\x70')  # GVDD 4.5V 1.0uA
        self._send(b'\xc1', b'\x05')      # Defaults
        self._send(b'\xc2', b'\xfa\x00')
        self._send(b'\xc3', b'\x02\x07')
        self._send(b'\xc4', b'\x02\x04')
        self._send(b'\xc5', b'\x51\x4d')

        # Settings
        self._send(b'\b4', b'\x07')  # INVCTR Frame Inversion
        self.inversion(False)        # INVOFF
        self.refreshOrder()          # MemoryDataAccessCTRL
        self._send(b'\x13')
        self.pixelFormat(0)          # COLMOD
        self.display(True)           # DISPON

    @micropython.native
    def reset(self, hw=True, sw=True, init=True):
        """
        reset display
        """
        if self._rst and hw:
            # Hardware Rest active = Low
            self._rst.value(ST7735.LOW)
            self._wait(0.12)
            self._rst.value(ST7735.HIGH)
            self._wait(0.12)  # wait 120 ms if in sleep mode
        elif hw:
            sw = True  # try soft-reset: prosthesis
        if sw:
            self._send(ST7735.CMD_SW_RESET)
            self._wait(0.12)
        if init:
            self.init()

    @micropython.native
    def sleep(self, sleep=True):
        """
        go in sleep mode?
        when it sleeps the display will not update the screen
        sleep: True means sleep, False is for wake up
        @ 10.1.10ff
        """
        if sleep:
            self._send(ST7735.CMD_SLEEP_IN)
        else:
            self._send(ST7735.CMD_SLEEP_OUT)
        self._wait(.12)

    @micropython.native
    def idle(self, idle=True):
        """
        go in idle mode?
        @ page 112ff
        """
        if idle:
            self._send(ST7735.CMD_IDLE_ON)
        else:
            self._send(ST7735.CMD_IDLE_OFF)
        self._wait(.12)

    @micropython.native
    def display(self, on=True):
        """
        switch display on/off
        on: hmmz, perhaps...
        """
        if on:
            self._send(ST7735.CMD_DISPLAY_ON)
        else:
            self._send(ST7735.CMD_DISPLAY_OFF)

    @micropython.native
    def inversion(self, on=True):
        """
        inversion on/off
        on: True -> inversion mode, False -> normal mode
        """
        if on:
            self._send(ST7735.CMD_INVERSON_ON)
        else:
            self._send(ST7735.CMD_INVERSON_OFF)

    def refreshOrder(self, topBottom=None, leftRight=None, colorRGB=None):
        """
        set the refresh order
        topBottom = True, else Bottom-Top; None = No Change
        leftRight = True, else Right-Left; None = No Change
        colorRGB = True for RGB, else BGR; None = No Change

        if the parameter is None, use the actual state
        """
        if topBottom is not None:
            self._order_TB = topBottom

        if leftRight is not None:
            self._order_LR = leftRight

        if colorRGB is not None:
            self._order_RGB = colorRGB

        dta = self._mirror
        if self._order_LR:
            dta += 4
        if self._order_RGB:
            dta += 8
        if self._order_TB:
            dta += 16
        dta = b'' + chr(dta)
        self._send(ST7735.CMD_REFRESH_ORDER, dta)

    def rotation(self, vertical=None, horizontal=None):
        """
        flip vertical and/or horizontal
        vertical = mirror? None = No Change
        horizontal = mirror? None = No Change
        """
        if vertical is not None:
            if vertical:
                self._mirror ^= 128

        if horizontal is not None:
            if horizontal:
                self._mirror ^= 64

        self.refreshOrder()

    def pixelFormat(self, pxlFormat=0):
        """
        define the color dept
        pxlFormat = see CLASS.OPT_COL_*; 0 = no change

        """
        if pxlFormat > 2 and pxlFormat < 7:
            self._pxlFormat = pxlFormat
        dta = b'' + chr(self._pxlFormat)
        self._send(ST7735.CMD_COLMOD, dta)
        return self._pxlFormat

    @micropython.native
    def fill_rect(self, sx=0, sy=0, ex=1, ey=1, clr=0):
        """
        Draw a filled rect from start-pos to stop-pos
        sx: start X in absolute values
        sy: start Y in absolute values
        ex: width
        ey: height
        clr: drawing color
        """

        sz = (1 + ex - sx) * (ey - sy + 1)
        sz = ey * ex
        ex += sx
        ey += sy
        if type(clr) is int:
            clr = clr.to_bytes(2, 'big')
        clr = clr * sz

        sx = sx.to_bytes(2, 'big') + ex.to_bytes(2, 'big')
        self._send(ST7735.CMD_SET_COLUMN, sx)

        sy = sy.to_bytes(2, 'big') + ey.to_bytes(2, 'big')
        self._send(ST7735.CMD_SET_ROW, sy)

        self._send(ST7735.CMD_WRITE_MEM, clr)

    @micropython.native
    def circle(self, x, y, r, clr):
        """
        Draw a non filled Circle
        x: center position: X
        y: center position: Y
        r: radius
        clr: color value
        """
        self.pixel(x, y + r, clr)
        self.pixel(x, y - r, clr)
        self.pixel(x + r, y, clr)
        self.pixel(x - r, y, clr)

        s = 1 - r
        sx = 0
        sy = r * -2
        xx = 0
        yy = r
        while xx < yy:
            if not s < 0:
                yy -= 1
                sy += 2
                s += sy
            xx += 1
            sx += 2
            s += 1 + sx
            self.pixel(x + xx, y + yy, clr)
            self.pixel(x + xx, y - yy, clr)
            self.pixel(x + yy, y + xx, clr)
            self.pixel(x + yy, y - xx, clr)
            self.pixel(x - xx, y + yy, clr)
            self.pixel(x - xx, y - yy, clr)
            self.pixel(x - yy, y + xx, clr)
            self.pixel(x - yy, y - xx, clr)

    @micropython.native
    def pixel(self, x=0, y=0, clr=0):
        """
        draw a pixel on screen
        x: x-coordinate
        y: y-coordinate
        clr: color
        """
        if type(clr) is int:
            clr = clr.to_bytes(2, 'big')
        x = x.to_bytes(2, 'big') * 2
        self._send(ST7735.CMD_SET_COLUMN, x)

        y = y.to_bytes(2, 'big') * 2
        self._send(ST7735.CMD_SET_ROW, y)

        self._send(ST7735.CMD_WRITE_MEM, clr)

    @micropython.native
    def draw(self, puf, x=0, y=0, w=0, h=0):
        """
        Draw a ColorBuffer
        use it for framebuf
        x: start position: X (default = 0)
        y: start position: Y (default = 0)
        w: width of the buffer (default = display width)
        h: height of the buffer(default = display height)
        puf: raw data
        """
        if w == 0:
            w = self._width
        if h == 0:
            h = 1 + len(puf)//w

        x = x.to_bytes(2, 'big') + w.to_bytes(2, 'big')
        self._send(ST7735.CMD_SET_COLUMN, x)

        y = y.to_bytes(2, 'big') + h.to_bytes(2, 'big')
        self._send(ST7735.CMD_SET_ROW, y)

        self._send(ST7735.CMD_WRITE_MEM, puf)

    @micropython.native
    def show(self):
        """
        Necessary for FrameBuffer
        Apply drawings (from FrameBuffer) to LCD
        """
        if self.__fb:
            if self.__offset:
                self.__offset.blit(self.__fb, self._oX, self._oY)
            self.draw(self.__fb_puf, 0, 0, self._width, self._height)
            return True
        else:
            return False
